﻿using Email_JWT.Models;

namespace Email_JWT.Services.EmailService
{
	public interface IEmailService
	{
		void SendEmail(EmailModel request);
	}
}
