﻿using Email_JWT.Models;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using MailKit.Security;

namespace Email_JWT.Services.EmailService
{
	public class EmailService : IEmailService
	{
		private readonly IConfiguration _configuration;
		public EmailService(IConfiguration configuration)
		{
			_configuration = configuration;	
		}

		public void SendEmail(EmailModel request)
		{
			var email = new MimeMessage();
			email.From.Add(MailboxAddress.Parse(_configuration.GetSection("EmailUserName").Value));
			email.To.Add(MailboxAddress.Parse(request.To));
			email.Subject = request.Subject;
			email.Body = new TextPart(TextFormat.Html) { Text = request.Body };

			var smtp = new SmtpClient();
			//smtp.Connect(_configuration.GetSection("EmailHost").Value, Convert.ToInt32(_configuration.GetSection("EmailPort").Value), SecureSocketOptions.StartTls);
			smtp.Connect(_configuration.GetSection("EmailHost").Value, Convert.ToInt32(_configuration.GetSection("EmailPort").Value), useSsl:true);
			smtp.Authenticate(_configuration.GetSection("EmailUserName").Value, _configuration.GetSection("EmailPassword").Value);
			smtp.Send(email);
			smtp.Disconnect(true);

		}
	}
}
