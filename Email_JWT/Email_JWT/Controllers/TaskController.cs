﻿using Email_JWT.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Data;

namespace Email_JWT.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TaskController : ControllerBase
	{
		private readonly IConfiguration _configuration;
		public TaskController(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		[HttpGet]
		[Authorize(Roles = "admin,sales")]
		[Route("GetTasksItem")]
		public IActionResult GetTaskItem()
		{
			List<TaskModel> tasks = new List<TaskModel>();
			MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection"));

			conn.Open();
			string query = "SELECT * FROM tasks";
			MySqlCommand cmd = new MySqlCommand(query, conn);
			MySqlDataAdapter dtAdapter = new MySqlDataAdapter(cmd);
			DataTable dt = new DataTable();
			dtAdapter.Fill(dt);

			foreach (DataRow dr in dt.Rows)
			{
				TaskModel task = new TaskModel();
				task.pk_tasks_id = Convert.ToInt32(dr["pk_tasks_id"]);
				task.tasks_detail = dr["taks_detail"].ToString();
				task.fk_users_id = Convert.ToInt32(dr["fk_users_id"]);
			}

			conn.Close();

			return Ok(tasks);
		}
	}
}
