﻿using Email_JWT.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Email_JWT.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class LoginController : ControllerBase
	{
		private readonly IConfiguration _configuration;
		public LoginController(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		[HttpPost]
		[Route("user")]
		public IActionResult Login([FromBody] LoginModel userLogin)
		{
			var user = ValidateUser(userLogin);
			if (user != null)
			{
				var token = GenerateToken(user);
				return Ok(token);
			}
			return NotFound("User Not Found");
		}

		private UserModel ValidateUser(LoginModel userLogin)
		{
			List<UserModel> users = new List<UserModel>();

			using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
			{
				conn.Open();
				string query = "Select * from tbl_user";
				MySqlCommand cmd = new MySqlCommand(query, conn);
				MySqlDataAdapter dtAdapter = new MySqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				dtAdapter.Fill(dt);

				foreach (DataRow dr in dt.Rows)
				{
					UserModel user = new UserModel();
					user.UserId = Convert.ToInt32(dr["UserId"].ToString());
					user.FirstName = dr["FirstName"].ToString();
					user.LastName = dr["LastName"] == (object)DBNull.Value ? "" : dr["LastName"].ToString();
					user.UserName = dr["UserName"] == (object)DBNull.Value ? "" : dr["UserName"].ToString();
					user.Email = dr["Email"] == (object)DBNull.Value ? "" : dr["Email"].ToString();
					user.Password = dr["Password"] == (object)DBNull.Value ? "" : dr["Password"].ToString();
					user.UserRole = dr["UserRole"] == (object)DBNull.Value ? "" : dr["UserRole"].ToString();
					user.IsActive = dr["IsActive"] == (object)DBNull.Value ? false : Convert.ToBoolean(dr["IsActive"]);
					users.Add(user);
				}

				conn.Close();
			}
			string hashPassword = BCrypt.Net.BCrypt.HashPassword(userLogin.Password);
			var currentUser = users.FirstOrDefault(u => u.UserName == userLogin.UserName &&
			BCrypt.Net.BCrypt.Verify(u.Password, hashPassword));

			if (currentUser != null)
			{
				return currentUser;
			}
			return null;
		}
		private string GenerateToken(UserModel user)
		{
			var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
			var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

			var claims = new[]
			{
				new Claim(ClaimTypes.NameIdentifier, user.UserName),
				new Claim(ClaimTypes.Email, user.Email),
				new Claim(ClaimTypes.Role, user.UserRole)
			};

			var token = new JwtSecurityToken(
				_configuration["Jwt:Issuer"],
				_configuration["Jwt:Audience"],
				claims,
				expires: DateTime.Now.AddDays(1),
				signingCredentials: credentials);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}

	}
}
