﻿using Email_JWT.Models;
using Email_JWT.Services.EmailService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace Email_JWT.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class RegisterController : ControllerBase
	{
		private readonly IConfiguration _configuration;
		private readonly IEmailService _emailService;
		public RegisterController(IConfiguration configuration, IEmailService emailService)
		{
			_configuration = configuration;
			_emailService = emailService;
		}

		[HttpPost]
		[Route("user")]
		public IActionResult RegisterUser([FromBody] UserModel user)
		{
			try
			{
				using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
				{
					conn.Open();
					string query = "Insert into tbl_User (`FirstName`, `LastName`, `UserName`, `Email`, `Password`, `UserRole`, `IsActive`) Values (@FirstName, @LastName, @UserName, @Email, @Password, @UserRole, @IsActive)";
					MySqlCommand cmd = new MySqlCommand(query, conn);
					cmd.Parameters.AddWithValue("@FirstName", user.FirstName);
					cmd.Parameters.AddWithValue("@LastName", user.LastName);
					cmd.Parameters.AddWithValue("@UserName", user.UserName);
					cmd.Parameters.AddWithValue("@Email", user.Email);
					cmd.Parameters.AddWithValue("@Password", user.Password);
					cmd.Parameters.AddWithValue("@UserRole", user.UserRole);
					cmd.Parameters.AddWithValue("@IsActive", false);
					cmd.ExecuteNonQuery();
					conn.Close();
				}
				//Generate Token
				var token = GenerateToken(user);

				//Sending Email
				EmailModel request = new EmailModel();
				//request.To = user.Email;
				request.To = "hanifmaziz@gmail.com";
				request.Subject = "Verify Account";
				request.Body = "<a href=\"http://localhost:41648/api/register/verify?token=[LINK]\">Verify This Link</a>".Replace("[LINK]", token);

				_emailService.SendEmail(request);

				return Ok("User Successfully Added");
			}
			catch (Exception ex)
			{
				return Ok(ex.Message);
			}
		}

		[HttpGet]
		[Route("verify")]
		public IActionResult Verify(string token)
		{
			bool validate = ValidateToken(token);

			if (validate)
			{
				var jwt = new JwtSecurityTokenHandler().ReadJwtToken(token);
				var user = jwt.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

				string query = "Update tbl_User set isActive =1 where UserName= @UserName";
				MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection"));
				conn.Open();
				MySqlCommand cmd = new MySqlCommand(query, conn);
				cmd.Parameters.AddWithValue("@UserName", user);
				cmd.ExecuteNonQuery();
				conn.Close();

				return Ok("Your Account Has Already Active");
			}
			else
			{
				return Ok("You are not authorized");
			}
		}

		private string GenerateToken(UserModel user)
		{
			var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
			var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

			var claims = new[]
			{
				new Claim(ClaimTypes.NameIdentifier, user.UserName),
				new Claim(ClaimTypes.Email, user.Email),
				new Claim(ClaimTypes.Role, user.UserRole)
			};

			var token = new JwtSecurityToken(
				_configuration["Jwt:Issuer"],
				_configuration["Jwt:Audience"],
				claims,
				expires: DateTime.Now.AddDays(1),
				signingCredentials: credentials);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}
		private bool ValidateToken(string token)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			var validationParameter = GetValidationParameters();

			SecurityToken validatedToken;
			try
			{
				IPrincipal principal = tokenHandler.ValidateToken(token, validationParameter, out validatedToken);
				if (principal.Identity.IsAuthenticated)
				{
					return true;
				}
				return false;
			}
			catch
			{
				return false;
			}
		}
		private TokenValidationParameters GetValidationParameters()
		{
			return new TokenValidationParameters()
			{
				ValidateIssuer = true,
				ValidateAudience = true,
				ValidateLifetime = true,
				ValidIssuer = _configuration["Jwt:Issuer"],
				ValidAudience = _configuration["Jwt:Audience"],
				IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]))
			};
		}

	}
}
