﻿namespace Email_JWT.Models
{
	public class TaskModel
	{
		public int pk_tasks_id { get; set; }
		public string? tasks_detail { get; set; }
		public int fk_users_id { get; set; }
	}
}
